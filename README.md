# jpackage-demo

Builds an JavaFX-Application with jlink and jpackage, mixing modules and normal jars.

## Building

To build a "normal" jar: 
```shell script
mvn clean package
```

To build a native application with jlink and jpackage
```shell script
mvn clean package -Papp
```
