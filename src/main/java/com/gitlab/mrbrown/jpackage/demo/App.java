package com.gitlab.mrbrown.jpackage.demo;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import org.ini4j.Ini;

public class App extends Application {

    @Override
    public void start(Stage stage) {
        Pane root = new Pane();
        // using Ini.class to test access
        root.getChildren().add(new Label("Hello World\n"+Ini.class));
        Scene scene = new Scene(root);
        stage.setTitle("Hello World");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String... args) {
        launch(args);
    }

}
