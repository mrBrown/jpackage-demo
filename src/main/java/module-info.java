module com.gitlab.mrbrown.jpackage.demo {
    requires javafx.controls;
    requires javafx.fxml;
    requires ini4j;
    exports com.gitlab.mrbrown.jpackage.demo to javafx.graphics;
    opens com.gitlab.mrbrown.jpackage.demo to javafx.fxml;
}
